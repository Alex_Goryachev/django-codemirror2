#!/usr/bin/env python3

import os
import sys

from setuptools import find_packages, setup


version = "0.2.1.dev1"

if sys.argv[-1] == "publish":
    os.system("python setup.py sdist upload")
    os.system("python setup.py bdist_wheel upload")
    sys.exit()

if sys.argv[-1] == "tag":
    os.system(f"git tag -a {version} -m 'version {version}'")
    os.system("git push --tags")
    sys.exit()

long_description = "\n".join(
    [
        open("README.rst").read()
    ]
)


setup(
    name="django-codemirror2",
    version=version,
    description="Django admin CKEditor integration.",
    long_description=long_description,
    author="Alexander Clausen",
    author_email="alex@gc-web.de",
    url="https://github.com/sk1p/django-codemirror2",
    project_urls={
        "Source": "https://github.com/sk1p/django-codemirror2",
    },
    packages=find_packages(exclude=["examples", "tests"]),
    zip_safe=False,
    install_requires=[
        "Django<6",
    ],
    python_requires=">= 3.10",
    include_package_data=True,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 3.2",
        "Framework :: Django :: 4.2",
        "Framework :: Django :: 5.0",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
        "Programming Language :: JavaScript",
        "Topic :: Text Editors",
    ],
)